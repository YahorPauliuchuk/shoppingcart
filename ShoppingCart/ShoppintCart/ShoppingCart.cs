﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCart.ShoppintCart.Models;
using ShoppingCart.EventFeed;

namespace ShoppingCart.ShoppintCart
{
    public class ShoppingCart
    {
        private HashSet<ShoppingCartItem> _items = new HashSet<ShoppingCartItem>();

        public int UserId { get; }

        public IEnumerable<ShoppingCartItem> Items { get { return _items; } }

        public ShoppingCart(int userId)
        {
            this.UserId = userId;
        }

        public void AddItems(
            IEnumerable<ShoppingCartItem> shoppingCartItems,
            IEventStore eventsStore)
        {
            foreach (var item in shoppingCartItems)
            {
                if (_items.Add(item))
                {
                    eventsStore.Raise(
                        "ShoppingCartItemAdded",
                        new { UserId, item });
                }
            }

        }

        public void RemoveItems(
            int[] productCatalogueIds,
            IEventStore eventStore)
        {
            _items.RemoveWhere(i => productCatalogueIds.Contains(i.ProductCatalogueId));
        }
    }
}
