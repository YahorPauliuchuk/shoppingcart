﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingCart.ShoppintCart.Models;

namespace ShoppingCart
{
    public interface IProductCatalogClient
    {
        Task<IEnumerable<ShoppingCartItem>> GetShoppingCartItems(int[] productCatalogIds);
    }
}
